const tasks = [{
  time: '08:30 AM',
  type: 'Running',
  icon: {
    type: 'directions_run',
    color: '#F581C0'
  }
},{
  time: '10:00 AM',
  type: 'Reading',
  icon: {
    type: 'import_contacts',
    color: '#8EBBF7'
  }
},{
  time: '08:30 AM',
  type: 'Buying',
  icon: {
    type: 'shopping_cart',
    color: '#BD88F1'
  }
},{
  time: '08:30 AM',
  type: 'Traveling',
  icon: {
    type: 'flight_takeoff',
    color: '#FFA874'
  }
}]

export default tasks