import React from 'react';
import { List } from './components/Task';
import tasks from './utils/tasks'
class App extends React.Component {
  state = {
    tasks
  }
  addTask = (task) => {
    const { tasks } = this.state;
    tasks.push(task)
    this.setState({
      tasks
    })
  }
  render(){
    const { tasks } = this.state
    const { addTask } = this;
    return (
      <div className="App">
        <List tasks = {tasks} addTask = {addTask}/>
      </div>
    );
  }
}

export default App;
