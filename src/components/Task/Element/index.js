import React from 'react'

import {
  ElementWrapper,
  MainContent,
  TaskTime,
  TaskType,
  TaskIcon,
  TaskTitle,
  TaskDescription
} from './styles'

const Element = ({
    task: {
      icon: { color, type },
      ...task
    }}
  ) => (
  <ElementWrapper>
    <TaskIcon color = {color}>{type}</TaskIcon>
    <MainContent>
      <TaskTitle>
        <TaskTime>
          {task.time}
        </TaskTime>
        <TaskType>
          {task.type}
        </TaskType>
      </TaskTitle>
      <TaskDescription>
        {task.type} for an hour
      </TaskDescription>
    </MainContent>
  </ElementWrapper>
)

export default Element;