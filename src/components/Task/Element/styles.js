import styled from 'styled-components'
import Icon from '../../Icon';

const TaskTitle = styled.span`
  font-family: 'Roboto', sans-serif;
  font-size: 18px;
  color: #8F8F91;
`
const TaskTime = styled.span`
`

const TaskType = styled.span`
`

const TaskDescription = styled.div`
  margin-top: 14px;
  font-family: 'Roboto', sans-serif;
  font-weight: 300;
  color: #8F8F91;
  letter-spacing: 1px;
`

const ElementWrapper = styled.div`
  display: flex;
  padding: 20px;
`

const TaskIcon = styled(Icon)`
  font-size: 40px;
  color: ${({color}) => color};
`

const MainContent = styled.div`
  margin-left: 20px;
  ${TaskTime} , ${TaskType} {
    margin-right: 8px;
    :last-child{
      margin-right: 0;
    }
  }
`


export {
  ElementWrapper,
  MainContent,
  TaskTime,
  TaskType,
  TaskIcon,
  TaskTitle,
  TaskDescription
}