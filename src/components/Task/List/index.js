import React from 'react';
import Element from '../Element';

import { ListWrapper } from './styles'

const options = [{
  type: 'Running',
  icon: 'directions_run'
},{
  type: 'Reading',
  icon: 'import_contacts'
},{
  type: 'Tipo',
  icon: 'delete'
}]

class AddElement extends React.Component {
  state = {
    time: '',
    type: 'Tipo',
    color: ''
  }
  _handleOnChange = ({target: {value, name}}) => {
    this.setState({
      [name]: value
    })
  }
  _handleSubmit = (e) => {
    const { addTask } = this.props;
    e.preventDefault();
    const { time, type, color } = this.state
    if(type === 'Tipo') return;
    const searchColorType = (type) => {
      return options.filter((option) => option.type === type)[0].icon
    }
    const task = {
      time: time,
      type: type,
      icon: {
        color: color,
        type: searchColorType(type)
      }
    }
    addTask(task)
  }
  render(){
    const { time , type ,color } = this.state;
    const { _handleOnChange, _handleSubmit } = this;
    return(
      <form onSubmit = {_handleSubmit}>
        <input 
          placeholder = 'Hora'
          name = "time" 
          type = "text" 
          value = {time} 
          onChange = {_handleOnChange}/>
        <select name = "type" value = {type} onChange = {_handleOnChange}>
          {options.map((option,index) => (
            <option value={option.type} key = {index}>{option.type}</option>
          ))}
        </select>
        <input 
          placeholder = "Color"
          name = "color" 
          type = "text" 
          value = {color} 
          onChange = {_handleOnChange}/>
        <input type = "submit" value = "Crear"/>
      </form>
    )
  }
}


const List = ({tasks , addTask}) => (
  <ListWrapper>
    <div style = {{
      height: 180,
      backgroundColor: '#BD79EA'
    }}>
      <AddElement addTask = {addTask}/>
    </div>
    {tasks.map((task, index) => (
      <Element task = {task} key = {index}/>
    ))}
  </ListWrapper>
)
export default List